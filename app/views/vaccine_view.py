from flask import Blueprint, request, current_app, jsonify
from app.models.vaccine_model import VaccineModel
from app.models.error_model import CpfError
from datetime import datetime, timedelta


bp = Blueprint('vaccine_card', __name__, url_prefix='/api')


@bp.post('/vaccination')
def create():
    data = request.get_json()

    first_date = datetime.now()
    second_date = first_date + timedelta(90)
    vaccine_cpf = data['cpf']

    
    try:
        if len(vaccine_cpf) != 11 or not vaccine_cpf.isdecimal():
            raise CpfError()

        new_vaccine = VaccineModel(
            cpf=vaccine_cpf,
            name=data['name'],
            vaccine_name=data['vaccine_name'],
            health_unit_name=data['health_unit_name'],
            first_shot_date=first_date,
            second_shot_date=second_date
        )

        session = current_app.db.session

        session.add(new_vaccine)
        session.commit()

        return jsonify(new_vaccine)
    
    except CpfError as err:
        return err.message


@bp.get('/vaccination')
def get_vaccines():
    query = VaccineModel.query.all()

    return jsonify(query), 200