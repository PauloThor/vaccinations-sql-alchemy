class CpfError(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.message = {
            "message": "CPF must have 11 numeric characters"
        }, 400        